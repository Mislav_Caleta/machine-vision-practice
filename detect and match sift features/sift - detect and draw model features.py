import cv2 as cv
import matplotlib.pyplot as plt
import random

"""load template image and turn to grayscale"""

src = cv.imread("src.jpg")
gray = cv.cvtColor(src, cv.COLOR_BGR2GRAY)

"""create sift object and detect keypoints and calculate descriptors on 
   template image to create a model consisting from features"""
   
sift = cv.xfeatures2d.SIFT_create()
modelKeypoints, modelDescriptors = sift.detectAndCompute(gray, None)

"""draw keypoints on model image"""

for point in modelKeypoints:
    x = int(point.pt[0])
    y = int(point.pt[1])
    b = random.randint(0, 256)
    g = random.randint(0, 256)
    r = random.randint(0, 256)
    cv.circle(src, (x, y), 15, (b, g, r), 3)

"""show drawn keypoints on model image"""

cv.namedWindow("keypoints", cv.WINDOW_NORMAL)
cv.resizeWindow("keypoints", 600, 700)
cv.imshow("keypoints", src)
cv.waitKey(0)
cv.destroyAllWindows()        