import cv2

"""
read template image and real image, show them and turn to grayscale
"""

template = cv2.imread("template.jpg")
image = cv2.imread("image.jpg")

tempWindow = "template image"
cv2.namedWindow(tempWindow, cv2.WINDOW_NORMAL)
cv2.resizeWindow(tempWindow, 600, 700)
cv2.imshow(tempWindow, template)

imgWindow = "image to detect on"
cv2.namedWindow(imgWindow, cv2.WINDOW_NORMAL)
cv2.resizeWindow(imgWindow, 600, 700)
cv2.imshow(imgWindow, image)

cv2.waitKey(0)
cv2.destroyAllWindows()

templateGray = cv2.cvtColor(template, cv2.COLOR_BGR2GRAY)
imageGray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

"""
detect sift features on template image and on real image
"""

sift = cv2.xfeatures2d.SIFT_create()

tempPoints, tempDesc = sift.detectAndCompute(templateGray, None)
imgPoints, imgDesc = sift.detectAndCompute(imageGray, None)

"""
find two best matches for image feature in template features (model)
"""

matcher = cv2.BFMatcher()
matches = matcher.knnMatch(imgDesc, tempDesc, k = 2)

"""
Lowe's ratio test, keep only good matches
"""

goodMatches = []
for match1, match2 in matches:
    if match1.distance < 0.75 * match2.distance:
        goodMatches.append([match1])
    
"""
Draw good matches
"""

matchImg = cv2.drawMatchesKnn(imageGray, imgPoints, templateGray, tempPoints, 
                           goodMatches, None, (0, 0, 255))

matchWindow = "matched features"
cv2.namedWindow(matchWindow)
cv2.imshow(matchWindow, matchImg)
cv2.waitKey(0)
cv2.destroyAllWindows()