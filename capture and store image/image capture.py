import cv2

#create VideoCapture object
cap = cv2.VideoCapture(0)

windowName = "capture"
cv2.namedWindow(windowName)

#check if camera is opened
if not cap.isOpened():
    print("cannot open camera")
    exit()

imgCounter = 0
while True:
    #read current frame
    ret, frame = cap.read()
    
    #check if frame is read
    if not ret:
        print("cannot recive frame")
        break
    
    #show current frame
    cv2.imshow(windowName, frame)
    
    #if q pressed exit video capture
    key = cv2.waitKey(1)
    if key == ord('q'):
        break
    
    #if c pressed capture image
    elif key == ord('c'):
        imgCounter += 1
        imgName = "image_" + str(imgCounter) + ".png"
        cv2.imwrite(imgName, frame)

#when done release capture
cap.release()
cv2.destroyAllWindows()
    



