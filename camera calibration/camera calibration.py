import numpy as np
import cv2

#prepare object points

objectPointsEx = []

for i in range(6):
    for j in range(8):
        point = []
        point.append(j)
        point.append(i)
        point.append(0)
        objectPointsEx.append(point)

objectPointsEx = np.array(objectPointsEx, dtype = np.float32)
print(objectPointsEx)

#arrays to store object points and image points from all the images
objectPoints = [] #3d points in real world space
imagePoints =[] #2d points in image plane

#take 10 images (of the calibration panel) and store their paths
images = []
cap = cv2.VideoCapture(0)
windowName = "capture"
cv2.namedWindow(windowName)

if not cap.isOpened():
    print("camera closed")
    exit()

imgNumber = 0
while imgNumber <= 9:
    ret, frame = cap.read()
    
    if not ret:
        print("can't get frame")
        break
    cv2.imshow(windowName, frame)
    
    key = cv2.waitKey(1)
    if key == ord('q'):
        break
    elif key == ord('c'):
        imgName = "img_" + str(imgNumber) + ".png"
        cv2.imwrite(imgName, frame)
        imgNumber += 1
        images.append(imgName)

cap.release()
cv2.destroyAllWindows()

#iterate trough all images that were captured
for image in images:
    img = cv2.imread(image)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    
    #find the chessboard (calibration panel) corners
    ret, corners = cv2.findChessboardCorners(gray, (8, 6), None)
    
    #If found, add object points and image points
    if ret == True:
        objectPoints.append(objectPointsEx)
        imagePoints.append(corners)
    
    #draw and display the corners
    cv2.drawChessboardCorners(img, (8, 6), corners, ret)
    cv2.imshow("chessboard", img)
    cv2.waitKey(0)

cv2.destroyAllWindows()

#calibrate camera, cameraMatrix is projection matrix
ret, cameraMatrix, distCoeffs, rvecs, tvecs, = cv2.calibrateCamera(objectPoints,
                                                              imagePoints,
                                                              gray.shape,
                                                              None, None)

#undistort the image
undistortedImg = cv2.undistort(img, cameraMatrix, distCoeffs)

cv2.imshow("undistorted img", undistortedImg)
cv2.waitKey(0)
cv2.destroyAllWindows()


    
        

